package com.fairytales.dataanalyser2021.utils;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Map;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import com.fairytales.dataanalyser2021.entity.FairyRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FairyTestUtils {

	
	public static FairyRequest getCorrectParametrizedRequestWithoutReqType() {
		FairyRequest request = new FairyRequest();
		LocalDateTime timeFrom = LocalDateTime.of(2021, 02, 01, 0, 0);
		LocalDateTime timeTo = LocalDateTime.of(2021, 02, 28, 23, 59);
		return request.setFromDate(timeFrom).setToDate(timeTo);
	}
	
	public static FairyRequest getParametrizedRequestWithRequestType(RequestTypeEnum requestTypeEnum) {
		return getCorrectParametrizedRequestWithoutReqType().setRequestType(requestTypeEnum);
	}
	
	public static Map<String, ?> requestObjectToParams(FairyRequest request) {
		ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> map = objectMapper.convertValue(request, Map.class);
        return map;
	}
}
