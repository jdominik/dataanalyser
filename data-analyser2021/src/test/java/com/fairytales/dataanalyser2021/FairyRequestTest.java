package com.fairytales.dataanalyser2021;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import com.fairytales.dataanalyser2021.entity.FairyRequest;
import com.fairytales.dataanalyser2021.utils.FairyTestUtils;

public class FairyRequestTest {
	
	@Test
	public void verifyData_basicRequest_validationTrue() {
		FairyRequest request = new FairyRequest();
		boolean isVerifiedOk = request.verifyData();
		assertThat(isVerifiedOk);
	}
	
	@Test
	public void verifyData_parametrizedRequest_validationTrue() {
		FairyRequest request = FairyTestUtils.getCorrectParametrizedRequestWithoutReqType();
		boolean isVerifiedOk = request.verifyData();
		assertThat(isVerifiedOk);
	}
	
	@Test
	public void verifyData_fromDateAfterToDateRequest_validationFalse() {
		FairyRequest request = FairyTestUtils.getCorrectParametrizedRequestWithoutReqType();
		LocalDateTime timeTo = LocalDateTime.of(2021, 01, 02, 23, 59);
		request.setToDate(timeTo);
		boolean isVerifiedOk = request.verifyData();
		assertThat(!isVerifiedOk);
	}
	
	@Test
	public void verifyData_nonNaturalNumberIntervalReuest_validationFalse() {
		FairyRequest request = FairyTestUtils.getCorrectParametrizedRequestWithoutReqType();
		request.setMinuteInterval(0);
		boolean isVerifiedOk = request.verifyData();
		assertThat(!isVerifiedOk);
		request.setMinuteInterval(-1);
		isVerifiedOk = request.verifyData();
		assertThat(!isVerifiedOk);
	}

}
