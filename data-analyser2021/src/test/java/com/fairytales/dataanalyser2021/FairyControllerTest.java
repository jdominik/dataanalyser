package com.fairytales.dataanalyser2021;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import com.fairytales.dataanalyser2021.entity.FairyRequest;
import com.fairytales.dataanalyser2021.manager.FairyDataRepository;
import com.fairytales.dataanalyser2021.utils.FairyTestUtils;
import com.fairytales.dataanalyser2021.utils.RequestTypeEnum;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class FairyControllerTest {
	
	private static final Logger LOG = LoggerFactory.getLogger(FairyControllerTest.class);
	
	private static final String INFO_API = "/api/1.0/info";
	private static final String ALL_PARAMS_REQ = "?fromDate={fromDate}&toDate={toDate}&minuteInterval={minuteInterval}";
	
	@Autowired
	TestRestTemplate testRestTemplate;
	
	@Autowired
	FairyDataRepository fairyDataRepository;

	
	@Test
	public void getLast_whenRequestWithoutParams_statusOk() {
		ResponseEntity<Object> response = testRestTemplate.getForEntity(INFO_API, Object.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void getLast_whenRequestWithoutParamsToLast_statusOk() {
		ResponseEntity<Object> response = testRestTemplate.getForEntity(INFO_API + "/" + RequestTypeEnum.LAST.getName(), Object.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void getAvgWithinTime_whenRequestWithoutParams_statusOk() {
		
		ResponseEntity<Object> response = testRestTemplate.getForEntity(INFO_API + "/" + RequestTypeEnum.AVG.getName(), Object.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void getGoodWithinTime_whenRequestWithoutParams_statusOk() {

		ResponseEntity<Object> response = testRestTemplate.getForEntity(INFO_API + "/" + RequestTypeEnum.GOOD.getName(), Object.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void getInterpolatedIntervalsWithinTime_whenRequestWithoutParams_statusOk() {
		
		ResponseEntity<Object> response = testRestTemplate.getForEntity(INFO_API + "/" + RequestTypeEnum.INTERPOLATE.getName(), Object.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void getLast_whenParametrizedRequest_statusOk() {
		FairyRequest parametrizedRequest = FairyTestUtils.getCorrectParametrizedRequestWithoutReqType();
		
		ResponseEntity<Object> response = testRestTemplate.getForEntity(INFO_API + ALL_PARAMS_REQ, Object.class, FairyTestUtils.requestObjectToParams(parametrizedRequest));
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		
		response = testRestTemplate.getForEntity(INFO_API + "/" + RequestTypeEnum.LAST.getName() + ALL_PARAMS_REQ, Object.class, FairyTestUtils.requestObjectToParams(parametrizedRequest));
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void getAvgWithinTime_whenParametrizedRequest_statusOk() {
		FairyRequest parametrizedRequest = FairyTestUtils.getCorrectParametrizedRequestWithoutReqType();
		
		ResponseEntity<Object> response = testRestTemplate.getForEntity(INFO_API + "/" + RequestTypeEnum.AVG.getName() + ALL_PARAMS_REQ, Object.class, FairyTestUtils.requestObjectToParams(parametrizedRequest));
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void getGoodWithinTime_whenParametrizedRequest_statusOk() {
		FairyRequest parametrizedRequest = FairyTestUtils.getCorrectParametrizedRequestWithoutReqType();
		
		ResponseEntity<Object> response = testRestTemplate.getForEntity(INFO_API + "/" + RequestTypeEnum.GOOD.getName() + ALL_PARAMS_REQ, Object.class, FairyTestUtils.requestObjectToParams(parametrizedRequest));
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void getInterpolatedIntervalsWithinTime_whenParametrizedRequestWithoutIntervalSpecified_statusOk() {
		FairyRequest parametrizedRequest = FairyTestUtils.getCorrectParametrizedRequestWithoutReqType();
		
		ResponseEntity<Object> response = testRestTemplate.getForEntity(INFO_API + "/" + RequestTypeEnum.INTERPOLATE.getName() + ALL_PARAMS_REQ, Object.class, FairyTestUtils.requestObjectToParams(parametrizedRequest));
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void getInterpolatedIntervalsWithinTime_whenParametrizedRequestWithIntervalSpecified_statusOk() {
		FairyRequest parametrizedRequest = FairyTestUtils.getCorrectParametrizedRequestWithoutReqType();
		parametrizedRequest.setMinuteInterval(5);
		
		ResponseEntity<Object> response = testRestTemplate.getForEntity(INFO_API + "/" + RequestTypeEnum.INTERPOLATE.getName() + ALL_PARAMS_REQ, Object.class, FairyTestUtils.requestObjectToParams(parametrizedRequest));
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	public void get_whenWrongEndpointRequest_statusNotFound() {
		FairyRequest parametrizedRequest = FairyTestUtils.getCorrectParametrizedRequestWithoutReqType();
		
		ResponseEntity<Object> response = testRestTemplate.getForEntity(INFO_API + "/notExisted" + ALL_PARAMS_REQ, Object.class, FairyTestUtils.requestObjectToParams(parametrizedRequest));
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}
	
	@Test
	public void getAvgWithinTime_whenRequestNotValidToDate_statusBadRequest() {
		FairyRequest request = FairyTestUtils.getCorrectParametrizedRequestWithoutReqType();
		LocalDateTime timeTo = LocalDateTime.of(2021, 01, 02, 23, 59);
		request.setToDate(timeTo);
		
		ResponseEntity<Object> response = testRestTemplate.getForEntity(INFO_API + "/" + RequestTypeEnum.AVG.getName() + ALL_PARAMS_REQ, Object.class, FairyTestUtils.requestObjectToParams(request));
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}
	
	@Test
	public void getInterpolatedIntervalsWithinTime_whenRequestNotValidInterval_statusBadRequest() {
		FairyRequest request = FairyTestUtils.getCorrectParametrizedRequestWithoutReqType();
		request.setMinuteInterval(0);
		
		ResponseEntity<Object> response = testRestTemplate.getForEntity(INFO_API + "/" + RequestTypeEnum.INTERPOLATE.getName() + ALL_PARAMS_REQ, Object.class, FairyTestUtils.requestObjectToParams(request));
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}

}
