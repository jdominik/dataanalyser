package com.fairytales.dataanalyser2021;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;

import com.fairytales.dataanalyser2021.entity.RawData;
import com.fairytales.dataanalyser2021.manager.FairyDataRepository;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class FairyDataTest {
	
	@Autowired
	FairyDataRepository fairyDataRepository;
	
	@Test
	public void getLast_whenNotParametrizedQuery_onlyOne() {
		List<RawData> result = fairyDataRepository.findFirst1ByOrderByTimeDesc();
		assertThat(result.size() == 1);
	}

}
