package com.fairytales.dataanalyser2021.utils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

@Component
public class FairyDateSerializer extends JsonSerializer<LocalDateTime> {


	@Override
	public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		//LocalDateTime formatDateTime = LocalDateTime.parse(value, formatter);
		if (value != null) {
			final String formattedDate = FairyUtils.formatter.format(value);
	        gen.writeString(formattedDate);
		}
	}
}
