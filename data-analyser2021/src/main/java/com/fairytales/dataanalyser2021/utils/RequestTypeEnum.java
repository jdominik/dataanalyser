package com.fairytales.dataanalyser2021.utils;

public enum RequestTypeEnum {
	LAST, 
	AVG, 
	GOOD, 
	INTERPOLATE;
	
	
	public String getName() {
        switch (this) {
            case LAST:
                return "last";
            case AVG:
                return "avg";
            case GOOD:
                return "good";
            case INTERPOLATE:
                return "interpolate";
        }
        return "last";
    }
	
	
}
