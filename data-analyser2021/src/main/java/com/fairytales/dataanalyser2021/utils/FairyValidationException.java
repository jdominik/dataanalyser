package com.fairytales.dataanalyser2021.utils;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Request not valid")
public class FairyValidationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8581971746691075547L;

	public FairyValidationException(String string) {
		super(string);
	}
	
}
