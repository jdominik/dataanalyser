package com.fairytales.dataanalyser2021.utils;

import java.io.IOException;
import java.time.LocalDateTime;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class FairyDateDeserializer extends JsonDeserializer<LocalDateTime> {

	@Override
	public LocalDateTime deserialize(JsonParser value, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		String date = value.getText();
		if (date != null) {
			return LocalDateTime.parse(date, FairyUtils.formatter);
		}
		return null;
	}

}
