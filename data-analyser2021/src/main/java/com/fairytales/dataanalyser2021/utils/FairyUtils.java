package com.fairytales.dataanalyser2021.utils;

import java.time.format.DateTimeFormatter;

public class FairyUtils {
	
	public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HHmmss");
	public static final String FROM_DATE = "fromDate";
	public static final String TO_DATE = "toDate";
	public static final String MINUTE_INTERVAL = "minuteInterval";
	public static final String PAGE = "page";
	public static final String SIZE = "size";
	public static final String IS_QUALITY = "isQuality";
}
