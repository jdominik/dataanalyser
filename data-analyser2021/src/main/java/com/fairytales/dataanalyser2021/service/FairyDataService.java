package com.fairytales.dataanalyser2021.service;


import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fairytales.dataanalyser2021.entity.FairyDataDTO;
import com.fairytales.dataanalyser2021.entity.RawData;
import com.fairytales.dataanalyser2021.manager.FairyDataRepository;
import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;

@Service
@Transactional
public class FairyDataService {
	
	FairyDataRepository fairyDataRepository;

	public FairyDataService(FairyDataRepository fairyDataRepository) {
		super();
		this.fairyDataRepository = fairyDataRepository;
	}
	
	public List<RawData> getLast(Boolean isQuality, Pageable pageable) {
		List<RawData> resultList = null;
		resultList = fairyDataRepository.findFirst1ByOrderByTimeDesc();
		return resultList;
	}

	public List<RawData> getAverage(LocalDateTime fromDate, LocalDateTime toDate, Boolean isQuality, Pageable pageable) {
		// above method not use isQuality if param is null
		List<RawData> resultList = fairyDataRepository.findAvgWithinDateTime(fromDate, toDate, isQuality);
		return resultList;
	}

	public List<RawData> getGood(LocalDateTime fromDate, LocalDateTime toDate, Boolean isQuality, Pageable pageable) {
		List<RawData> resultList = null;
		resultList = fairyDataRepository.findByTimeBetween(fromDate, toDate);

		return resultList;
	}

	public List<FairyDataDTO> getInterpolated(LocalDateTime fromDate, LocalDateTime toDate, Integer minuteInterval, Boolean isQuality, Pageable pageable) {
		List<RawData> resultList = fairyDataRepository.findByTimeBetween(fromDate, toDate);
		List<FairyDataDTO> interpValues = new LinkedList<>();
		if (!resultList.isEmpty()) {
			double[] timeArgs = null;
			double[] values = null;
			for(int i=0; i < resultList.size(); i++) {
	        	timeArgs[i] = (float) resultList.get(i).getTime().toInstant(null).getEpochSecond();
	        	values[i] = resultList.get(i).getValue().floatValue();
	        };
			
			LinearInterpolator linearInterpolator = new LinearInterpolator();
	        
			PolynomialSplineFunction psf = linearInterpolator.interpolate(timeArgs, values);
	        
			Instant beginInst = fromDate.toInstant(ZoneOffset.UTC);
			Instant endInst = toDate.toInstant(ZoneOffset.UTC);
			
			for (Instant date = beginInst; beginInst.isBefore(endInst); date = date.plusSeconds(minuteInterval*60)) {
				interpValues.add(new FairyDataDTO(LocalDateTime.ofInstant(date, ZoneOffset.UTC), (float) psf.value(date.getEpochSecond())));
			}
		}
		return interpValues;
	}


}
