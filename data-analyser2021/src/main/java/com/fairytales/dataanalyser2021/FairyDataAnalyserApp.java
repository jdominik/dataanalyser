package com.fairytales.dataanalyser2021;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FairyDataAnalyserApp {

	public static void main(String[] args) {
		SpringApplication.run(FairyDataAnalyserApp.class, args);
	}
	
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
