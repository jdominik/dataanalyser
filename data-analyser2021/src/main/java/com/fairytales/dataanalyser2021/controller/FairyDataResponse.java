package com.fairytales.dataanalyser2021.controller;

import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;

import com.fairytales.dataanalyser2021.entity.FairyDataDTO;
import com.fairytales.dataanalyser2021.entity.FairyRequest;
import com.fairytales.dataanalyser2021.entity.RawData;
import com.fairytales.dataanalyser2021.service.FairyDataService;
import com.fairytales.dataanalyser2021.utils.FairyValidationException;


public class FairyDataResponse extends FairyBasicResponse {
	
	private FairyDataDTO[] values;
	
	private ModelMapper mapper;
	
	protected FairyDataService fairyDataService;
		
	public FairyDataResponse(Pageable pageable) {
		values = getConvertedDTO(fairyDataService.getLast(null, pageable));
	}
	
	public FairyDataResponse(FairyRequest request, Pageable pageable, FairyDataService service, ModelMapper modelMapper) {
		super(request);
		this.fairyDataService = service;
		this.mapper = modelMapper;
		if (Boolean.TRUE.equals(this.getIsRequestValid())) {
			values = getProcessedData(request, pageable);
		} else {
			throw new FairyValidationException("Request not valid");
		}
	}
	
	protected FairyDataDTO[] getProcessedData(FairyRequest request, Pageable pageable) {
		List<RawData> rawList = null;
		switch (request.getRequestType()) {
		case AVG:
			rawList = fairyDataService.getAverage(request.getFromDate(), request.getToDate(), request.getIsQuality(), pageable);
		case GOOD:
			rawList = fairyDataService.getGood(request.getFromDate(), request.getToDate(), request.getIsQuality(), pageable);
		case INTERPOLATE:
			List<FairyDataDTO> dataList = fairyDataService.getInterpolated(request.getFromDate(), request.getToDate(), request.getMinuteInterval(), null, pageable);
			return dataList.toArray(FairyDataDTO[]::new);
		default:
			rawList = fairyDataService.getLast(null, pageable);
		}
		return getConvertedDTO(rawList);
	}
	
	private FairyDataDTO convertToDto(RawData raw) {
		FairyDataDTO dto = mapper.map(raw, FairyDataDTO.class);
	    return dto;
	}
	
	private FairyDataDTO[] getConvertedDTO(List<RawData> rawList) {
		return rawList.stream()
	    .map(this::convertToDto)
	    .toArray(FairyDataDTO[]::new);
	}
	

}