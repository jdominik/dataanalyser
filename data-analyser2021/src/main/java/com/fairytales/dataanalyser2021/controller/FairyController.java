package com.fairytales.dataanalyser2021.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fairytales.dataanalyser2021.entity.FairyRequest;
import com.fairytales.dataanalyser2021.service.FairyDataService;
import com.fairytales.dataanalyser2021.utils.FairyUtils;
import com.fairytales.dataanalyser2021.utils.RequestTypeEnum;

@RestController
@RequestMapping(value = "/api/1.0", method = RequestMethod.GET)
public class FairyController {
	
	private static final String PAGE_NO = "0";
	private static final String SIZE_NO = "100";
	
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	protected FairyDataService service;
	

	
	@GetMapping(value = {"/info", "/info/last"}, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public FairyBasicResponse getLast(
			Pageable page,
			@RequestParam(value = FairyUtils.FROM_DATE, required = false) String fromDate,
		    @RequestParam(value = FairyUtils.TO_DATE, required = false) String toDate,
		    @RequestParam(value = FairyUtils.MINUTE_INTERVAL, required = false) String minuteInterval) {
		return new FairyBasicResponse();
	}
	
	@GetMapping(path = "/info/avg", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public FairyDataResponse getAvgWithinTime(
			Pageable page,
			@RequestParam(value = FairyUtils.FROM_DATE, required = false, defaultValue = "2021-01-01T000000") String fromDate,
		    @RequestParam(value = FairyUtils.TO_DATE, required = false, defaultValue = "2021-02-01T000000") String toDate,
		    @RequestParam(value = FairyUtils.IS_QUALITY, required = false, defaultValue = "1") Boolean isQuality) {
		
		FairyRequest request = new FairyRequest(fromDate, toDate, isQuality, RequestTypeEnum.AVG);
		return new FairyDataResponse(request, page, service, mapper);
	}
	
	@GetMapping(path = "/info/good", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public FairyDataResponse getGoodWithinTime(
			Pageable page,
			@RequestParam(value = FairyUtils.FROM_DATE, required = false, defaultValue = "2021-01-01T000000") String fromDate,
		    @RequestParam(value = FairyUtils.TO_DATE, required = false, defaultValue = "2021-02-01T000000") String toDate,
		    @RequestParam(value = FairyUtils.MINUTE_INTERVAL, required = false) Integer minuteInterval) {
		
		FairyRequest request = new FairyRequest(fromDate, toDate, minuteInterval, RequestTypeEnum.GOOD);
		return new FairyDataResponse(request, page, service, mapper);
	}
	
	@GetMapping(path = "/info/interpolate", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public FairyDataResponse getInterpolatedIntervalsWithinTime(
			Pageable page,
			@RequestParam(value = FairyUtils.FROM_DATE, required = false, defaultValue = "2021-01-01T000000") String fromDate,
		    @RequestParam(value = FairyUtils.TO_DATE, required = false, defaultValue = "2021-02-01T000000") String toDate,
		    @RequestParam(value = FairyUtils.MINUTE_INTERVAL, required = false, defaultValue = "180") Integer minuteInterval) {
		
		FairyRequest request = new FairyRequest(fromDate, toDate, minuteInterval, RequestTypeEnum.INTERPOLATE);
		return new FairyDataResponse(request, page, service, mapper);
	}

}
