package com.fairytales.dataanalyser2021.controller;

import java.util.Date;

import com.fairytales.dataanalyser2021.entity.FairyRequest;

public class FairyBasicResponse {
	
	private Date requestDate;
	private boolean isRequestValid;
	
	public FairyBasicResponse() {
		this.requestDate = new Date();
		this.isRequestValid = true;
	}
	
	public FairyBasicResponse(FairyRequest request) {
		this.requestDate = new Date();
		this.isRequestValid = request.verifyData();
	}
	
	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	public boolean getIsRequestValid() {
		return isRequestValid;
	}
	public void setIsRequestValid(boolean isRequestValid) {
		this.isRequestValid = isRequestValid;
	}
	

}
