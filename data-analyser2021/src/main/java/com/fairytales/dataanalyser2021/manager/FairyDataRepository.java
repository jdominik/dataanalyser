package com.fairytales.dataanalyser2021.manager;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fairytales.dataanalyser2021.entity.RawData;


public interface FairyDataRepository extends JpaRepository<RawData, Long> {

	
	public List<RawData> findFirst1ByOrderByTimeDesc();

    @Query("SELECT AVG(raw.value) FROM RawData raw WHERE raw.time > :fromDate AND raw.time < :toDate and (:quality is null or raw.isQuality = :quality)") 
    public List<RawData> findAvgWithinDateTime(@Param("fromDate") LocalDateTime fromDate,
    		@Param("toDate") LocalDateTime toDate, @Param("quality") Boolean isQuality);
    
    public List<RawData> findByTimeBetween(LocalDateTime fromDate,
    		LocalDateTime toDate);


}
