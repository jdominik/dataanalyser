package com.fairytales.dataanalyser2021.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="FAIRY_PRESSURE_DATA")
public class RawData {
	
	@Id
	@Column(name="ID")
	private Long id;
	
	@Column(name="TIME")
	private LocalDateTime time;
	
	@Column(name="VALUE")
	private Float value;
	
	@Column(name="ENGINNERING_UNIT")
	private String engineeringUnit;
	
	@Column(name="QUALITY")
	private Boolean isQuality;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	public String getEngineeringUnit() {
		return engineeringUnit;
	}

	public void setEngineeringUnit(String engineeringUnit) {
		this.engineeringUnit = engineeringUnit;
	}

	public Boolean getIsQuality() {
		return isQuality;
	}

	public void setIsQuality(boolean isQuality) {
		this.isQuality = isQuality;
	}

	@Override
	public String toString() {
		return "RawData [id=" + id + ", time=" + time + ", value=" + value + ", engineeringUnit=" + engineeringUnit
				+ ", quality=" + isQuality + "]";
	}

}
