package com.fairytales.dataanalyser2021.entity;

import java.time.LocalDateTime;

import com.fairytales.dataanalyser2021.utils.FairyDateDeserializer;
import com.fairytales.dataanalyser2021.utils.FairyDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class FairyDataDTO {

	private LocalDateTime time;
	
	private Float value;
	
	private Boolean isQuality;
	
	public FairyDataDTO(LocalDateTime time, Float value) {
		this.time = time;
		this.value = value;
	}

	@JsonSerialize(using = FairyDateSerializer.class)
	public LocalDateTime getTime() {
		return time;
	}

	@JsonDeserialize(using = FairyDateDeserializer.class)
	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public Boolean getIsQuality() {
		return isQuality;
	}

	public void setIsQuality(Boolean isQuality) {
		this.isQuality = isQuality;
	}

}
