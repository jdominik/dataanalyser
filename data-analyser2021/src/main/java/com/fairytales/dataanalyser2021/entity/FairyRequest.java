package com.fairytales.dataanalyser2021.entity;

import java.time.LocalDateTime;

import com.fairytales.dataanalyser2021.utils.FairyDateDeserializer;
import com.fairytales.dataanalyser2021.utils.FairyDateSerializer;
import com.fairytales.dataanalyser2021.utils.FairyUtils;
import com.fairytales.dataanalyser2021.utils.FairyValidationException;
import com.fairytales.dataanalyser2021.utils.RequestTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class FairyRequest {

	private LocalDateTime fromDate;
	private LocalDateTime toDate;
	private Integer minuteInterval;
	private Boolean isQuality;
	@JsonIgnore
	private RequestTypeEnum requestType;
	
	public FairyRequest() {
		
	}
	
	public FairyRequest(String fromDateString, String toDateString, Integer minuteInterval,
			RequestTypeEnum requestType) {
		try {
			this.fromDate = fromDateString !=null ? LocalDateTime.parse(fromDateString, FairyUtils.formatter) : null;
			this.toDate = toDateString != null ? LocalDateTime.parse(toDateString, FairyUtils.formatter) : null;
		} catch (Exception e) {
			throw new FairyValidationException("Request dates cannot be parsed");
		}
		this.minuteInterval = minuteInterval;
		this.requestType = requestType;
	}

	public FairyRequest(String fromDateString, String toDateString, Boolean isQuality, RequestTypeEnum requestType) {
		try {
			this.fromDate = fromDateString !=null ? LocalDateTime.parse(fromDateString, FairyUtils.formatter) : null;
			this.toDate = toDateString != null ? LocalDateTime.parse(toDateString, FairyUtils.formatter) : null;
		} catch (Exception e) {
			throw new FairyValidationException("Request dates cannot be parsed");
		}
		this.isQuality = isQuality;
		this.requestType = requestType;
	}

	@JsonSerialize(using = FairyDateSerializer.class)
	public LocalDateTime getFromDate() {
		return fromDate;
	}
	
	@JsonDeserialize(using = FairyDateDeserializer.class)
	public FairyRequest setFromDate(LocalDateTime fromDate) {
		this.fromDate = fromDate;
		return this;
	}
	
	@JsonSerialize(using = FairyDateSerializer.class)
	public LocalDateTime getToDate() {
		return toDate;
	}
	
	@JsonDeserialize(using = FairyDateDeserializer.class)
	public FairyRequest setToDate(LocalDateTime toDate) {
		this.toDate = toDate;
		return this;
	}
	public Integer getMinuteInterval() {
		return minuteInterval;
	}
	public FairyRequest setMinuteInterval(int minuteInterval) {
		this.minuteInterval = minuteInterval;
		return this;
	}
	 public Boolean getIsQuality() {
		return isQuality;
	}
	public void setIsQuality(Boolean isQuality) {
		this.isQuality = isQuality;
	}

	public RequestTypeEnum getRequestType() {
		 return requestType;
	 } 
	 public FairyRequest setRequestType(RequestTypeEnum returnType){
		 this.requestType = returnType;
		 return this;
	 }
	 
	@Override
	public String toString() {
		return "FairyRequest [fromDate=" + fromDate + ", toDate=" + toDate + ", minuteInterval=" + minuteInterval
				+ ", returnType=" + requestType + "]";
	}
	public boolean verifyData() {
		boolean result = true;
		if (getFromDate() != null && getToDate() != null) {
			result &= getToDate().isAfter(getFromDate());	// check that toDate is after fromDate
		}
		if (getMinuteInterval() != null) {
			result &= getMinuteInterval() > 0;	// checks that getMinuteInterval() is not less than 1
		}
		return result;
	}
	
}
